<?php

require 'app/start.php';
require_once "core/init.php";
//paarbaudam vai linkaa ir noradits page
if (empty($_GET['page'])) {
	$page = false;
} else {
	$slug = $_GET['page'];

	$page = $db->prepare("
		SELECT *
		FROM pages 
		WHERE slug = :slug
	");

	$page->execute(['slug' => $slug]);
	$page = $page->fetch(PDO::FETCH_ASSOC);

	$page_id = $page['id'];
	
	$pageCommits = $db->prepare("
		SELECT *
		FROM attachments 
		WHERE page_id = :page_id
		ORDER BY created DESC
	");
	//echo $pageid;
	$pageCommits->execute(['page_id' => $page_id]);
	$pageCommits = $pageCommits->fetchAll();
	
	/*Lietotaja varda izgusanas logika kommitam, procesaa ar inner joiniem*/
	/*
	$id = $ticket['user_id'];
	$getUsername = $db->prepare("
		SELECT attachments.user_id, users.username 
		FROM attachments
		INNER JOIN users 
		ON attachments.user_id = users.user_id
		
	");

	$getUsername->execute(['id' => $id]);
	$getUsername = $getUsername->fetch(PDO::FETCH_ASSOC);
	*/

	if ($page) {
		
		$page['created'] = new DateTime($page['created']);
		if ($page['updated']) {
			$page['updated'] = new DateTime($page['updated']);
		}
	}

}

require VIEW_ROOT . '/page/show.php';

<?php
require 'app/start.php';
require_once 'core/init.php';
require VIEW_ROOT . '/templates/header.php';
	if(Input::exists()) {
		if(Token::check(Input::get('token'))) {
			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'username' => array('required' => true),
				'password' => array('required' => true)
			));

			if($validation->passed()) {
				$user = new User();

				$remember = (Input::get('remember') === 'on') ? true : false;
				//apstradajam login
				$login = $user->login(Input::get('username'), Input::get('password'), $remember);
				if($login) {
					Redirect::to('index.php');
				} else {
					echo '<p>Ielogošanās nesekmīga</p>';
				}

			} else {
				foreach($validation->errors() as $error) {
					echo $error, '<br>';
				}
			}
		}
	}
	?>
  <div class="row">
    <div class="col-md-6">
		<form action="" method="post">
			<div class="form-group row">
				<label for="username" class="col-sm-2 col-form-label">Lietotājvārds</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="username" id="username" autocomplete="off">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label" for="password">Parole</label>
				<div class="col-sm-10">
					<input class="form-control" type="password" name="password" id="password" autocomplete="off">
				</div>
			</div>

			<div class="form-group row">
				<div class="col-sm-2 col-form-label"></div>
				<div class="col-sm-10">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="remember" id="remember">
						<label class="form-check-label" for="remember">
			 			Atcerēties mani
						</label>
					</div>
				</div>
			</div>

			<input type="hidden" name="token" value="<?php echo Token::generate();?>">
			<input class="btn btn-primary" type="submit" value="Ieiet">
		</form>
	</div>
  </div>
<?php require VIEW_ROOT . '/templates/footer.php'; ?>
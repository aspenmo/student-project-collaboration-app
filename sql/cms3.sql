-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2019 at 11:50 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_latvian_ci NOT NULL,
  `permissions` text COLLATE utf8_latvian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`) VALUES
(1, 'Standard user', ''),
(2, 'Standard user', ''),
(3, 'Administrator', '{\"admin\": 1}');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) UNSIGNED NOT NULL,
  `label` varchar(20) COLLATE utf8_latvian_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_latvian_ci NOT NULL,
  `body` text COLLATE utf8_latvian_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_latvian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `label`, `title`, `body`, `slug`, `created`, `updated`) VALUES
(6, 'Aijas lapas piemÄ“rs', 'Aijas lapas piemÄ“rs', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis, nisi sit amet condimentum maximus, ex eros tristique lacus, quis efficitur lectus massa a elit. Nulla fringilla porta velit, nec gravida justo bibendum at. Vestibulum lorem quam, vehicula eu erat eget, placerat tincidunt purus. Sed laoreet erat sit amet massa faucibus maximus. Donec scelerisque suscipit felis, vel bibendum erat feugiat at. Donec tempus, diam at ornare condimentum, nibh erat euismod mauris, et elementum est mauris non eros. Proin dapibus laoreet mi, eget elementum eros.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam diam est, lacinia eu congue id, vestibulum id augue. Duis faucibus lobortis dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc at felis ut mauris imperdiet egestas sed sit amet tortor. Morbi et pellentesque augue, volutpat porttitor elit. Integer quis porttitor diam. Fusce facilisis lacus ante, ac sagittis odio vulputate vehicula. Pellentesque ultrices ex ut nibh fermentum consectetur. Mauris euismod pellentesque tellus nec ultrices. Donec tincidunt, tellus pellentesque porttitor dignissim, sapien neque elementum risus, ac commodo erat massa nec lectus. Maecenas ut lobortis eros, gravida finibus neque. Nam ultrices leo nec efficitur accumsan. Mauris non venenatis nisl. 1', 'aija-page', '2019-03-01 11:36:31', '2019-03-01 11:36:31'),
(7, 'Jura lapas piemÄ“rs', 'Jura lapas piemÄ“rs', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis, nisi sit amet condimentum maximus, ex eros tristique lacus, quis efficitur lectus massa a elit. Nulla fringilla porta velit, nec gravida justo bibendum at. Vestibulum lorem quam, vehicula eu erat eget, placerat tincidunt purus. Sed laoreet erat sit amet massa faucibus maximus. Donec scelerisque suscipit felis, vel bibendum erat feugiat at. Donec tempus, diam at ornare condimentum, nibh erat euismod mauris, et elementum est mauris non eros. Proin dapibus laoreet mi, eget elementum eros.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam diam est, lacinia eu congue id, vestibulum id augue. Duis faucibus lobortis dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc at felis ut mauris imperdiet egestas sed sit amet tortor. Morbi et pellentesque augue, volutpat porttitor elit. Integer quis porttitor diam. Fusce facilisis lacus ante, ac sagittis odio vulputate vehicula. Pellentesque ultrices ex ut nibh fermentum consectetur. Mauris euismod pellentesque tellus nec ultrices. Donec tincidunt, tellus pellentesque porttitor dignissim, sapien neque elementum risus, ac commodo erat massa nec lectus. Maecenas ut lobortis eros, gravida finibus neque. Nam ultrices leo nec efficitur accumsan. Mauris non venenatis nisl.', 'juris-page', '2019-03-01 11:36:53', NULL),
(8, 'Litas lapas piemÄ“rs', 'Litas lapas piemÄ“rs', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis, nisi sit amet condimentum maximus, ex eros tristique lacus, quis efficitur lectus massa a elit. Nulla fringilla porta velit, nec gravida justo bibendum at. Vestibulum lorem quam, vehicula eu erat eget, placerat tincidunt purus. Sed laoreet erat sit amet massa faucibus maximus. Donec scelerisque suscipit felis, vel bibendum erat feugiat at. Donec tempus, diam at ornare condimentum, nibh erat euismod mauris, et elementum est mauris non eros. Proin dapibus laoreet mi, eget elementum eros.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam diam est, lacinia eu congue id, vestibulum id augue. Duis faucibus lobortis dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc at felis ut mauris imperdiet egestas sed sit amet tortor. Morbi et pellentesque augue, volutpat porttitor elit. Integer quis porttitor diam. Fusce facilisis lacus ante, ac sagittis odio vulputate vehicula. Pellentesque ultrices ex ut nibh fermentum consectetur. Mauris euismod pellentesque tellus nec ultrices. Donec tincidunt, tellus pellentesque porttitor dignissim, sapien neque elementum risus, ac commodo erat massa nec lectus. Maecenas ut lobortis eros, gravida finibus neque. Nam ultrices leo nec efficitur accumsan. Mauris non venenatis nisl.', 'lita-page', '2019-03-01 11:37:17', NULL),
(9, 'Modris kodÄ“ lapsas', 'Modris kodÄ“ lapsas', ' Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam diam est, lacinia eu congue id, vestibulum id augue. Duis faucibus lobortis dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc at felis ut mauris imperdiet egestas sed sit amet tortor. Morbi et pellentesque augue, volutpat porttitor elit. Integer quis porttitor diam. Fusce facilisis lacus ante, ac sagittis odio vulputate vehicula. Pellentesque ultrices ex ut nibh fermentum consectetur. Mauris euismod pellentesque tellus nec ultrices. Donec tincidunt, tellus pellentesque porttitor dignissim, sapien neque elementum risus, ac commodo erat massa nec lectus. Maecenas ut lobortis eros, gravida finibus neque. Nam ultrices leo nec efficitur accumsan. Mauris non venenatis nisl.\r\n\r\nProin non laoreet nulla. Nulla congue tincidunt congue. Maecenas cursus pellentesque lacus, quis facilisis ligula tempus id. Aliquam vitae dui dolor. Quisque congue eleifend fringilla. Ut euismod orci dignissim vulputate viverra. Curabitur porta tincidunt leo ut fermentum. Aliquam venenatis leo in velit maximus ullamcorper. Nullam vel tempor dui, consequat luctus lacus. Aliquam nulla orci, molestie ac ornare quis, malesuada a nisl. Mauris dapibus orci quis neque dapibus, id luctus justo tristique. Praesent mattis tempor mauris ac mollis.\r\n\r\nAliquam nisi felis, accumsan nec rhoncus at, congue et dolor. Cras at luctus mi. Proin quis pharetra mauris. In non fermentum tellus. Curabitur sagittis pretium facilisis. Nullam eu maximus quam. Nulla nec nulla in ligula dictum mattis mattis at enim. Cras consectetur sollicitudin facilisis. Donec viverra lacinia nibh sit amet finibus. Vivamus in leo volutpat, finibus sapien nec, rutrum lacus. Phasellus vel massa et enim pulvinar tincidunt at nec odio. Curabitur nulla purus, accumsan id erat at, dapibus porttitor orci. Phasellus ac porttitor orci. Nulla nec fringilla tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'modris-page', '2019-03-01 11:37:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_latvian_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_latvian_ci NOT NULL,
  `salt` varchar(32) COLLATE utf8_latvian_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_latvian_ci NOT NULL,
  `joined` datetime NOT NULL,
  `usergroup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `name`, `joined`, `usergroup`) VALUES
(6, 'modris', '58b74243d409b7c4b89aae0b72d9cafdfcd82bcbcc8615a18719db0efd55560f', 'ƒ·^ljH[¹O	¡¼Œkn4¤ä qøï@-ó1^', 'Modris', '2019-04-23 23:44:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_session`
--

CREATE TABLE `users_session` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hash` varchar(50) COLLATE utf8_latvian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_session`
--
ALTER TABLE `users_session`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users_session`
--
ALTER TABLE `users_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2019 at 03:35 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE `attachments` (
  `id` int(11) NOT NULL,
  `url` varchar(300) COLLATE utf8_latvian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `page_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_latvian_ci NOT NULL,
  `permissions` text COLLATE utf8_latvian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`) VALUES
(1, 'Standard user', ''),
(2, 'Standard user', ''),
(3, 'Administrator', '{\r\n\"admin\": 1,\r\n\"moderator\": 0\r\n}'),
(4, 'Moderator', '{\r\n\"admin\": 0,\r\n\"moderator\": 1\r\n}');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) UNSIGNED NOT NULL,
  `label` varchar(20) COLLATE utf8_latvian_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_latvian_ci NOT NULL,
  `body` text COLLATE utf8_latvian_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8_latvian_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `label`, `title`, `body`, `slug`, `created`, `updated`, `user_id`) VALUES
(6, 'Unity spÄ“le', 'Aijas lapas piemÄ“rs', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis, nisi sit amet condimentum maximus, ex eros tristique lacus, quis efficitur lectus massa a elit. Nulla fringilla porta velit, nec gravida justo bibendum at. Vestibulum lorem quam, vehicula eu erat eget, placerat tincidunt purus. Sed laoreet erat sit amet massa faucibus maximus. Donec scelerisque suscipit felis, vel bibendum erat feugiat at. Donec tempus, diam at ornare condimentum, nibh erat euismod mauris, et elementum est mauris non eros. Proin dapibus laoreet mi, eget elementum eros.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam diam est, lacinia eu congue id, vestibulum id augue. Duis faucibus lobortis dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc at felis ut mauris imperdiet egestas sed sit amet tortor. Morbi et pellentesque augue, volutpat porttitor elit. Integer quis porttitor diam. Fusce facilisis lacus ante, ac sagittis odio vulputate vehicula. Pellentesque ultrices ex ut nibh fermentum consectetur. Mauris euismod pellentesque tellus nec ultrices. Donec tincidunt, tellus pellentesque porttitor dignissim, sapien neque elementum risus, ac commodo erat massa nec lectus. Maecenas ut lobortis eros, gravida finibus neque. Nam ultrices leo nec efficitur accumsan. Mauris non venenatis nisl. 1', 'aija-page', '2019-04-24 12:49:57', '2019-04-24 12:49:57', 0),
(7, 'SimulÄcija JavÄ', 'Jura lapas piemÄ“rs', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis, nisi sit amet condimentum maximus, ex eros tristique lacus, quis efficitur lectus massa a elit. Nulla fringilla porta velit, nec gravida justo bibendum at. Vestibulum lorem quam, vehicula eu erat eget, placerat tincidunt purus. Sed laoreet erat sit amet massa faucibus maximus. Donec scelerisque suscipit felis, vel bibendum erat feugiat at. Donec tempus, diam at ornare condimentum, nibh erat euismod mauris, et elementum est mauris non eros. Proin dapibus laoreet mi, eget elementum eros.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam diam est, lacinia eu congue id, vestibulum id augue. Duis faucibus lobortis dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc at felis ut mauris imperdiet egestas sed sit amet tortor. Morbi et pellentesque augue, volutpat porttitor elit. Integer quis porttitor diam. Fusce facilisis lacus ante, ac sagittis odio vulputate vehicula. Pellentesque ultrices ex ut nibh fermentum consectetur. Mauris euismod pellentesque tellus nec ultrices. Donec tincidunt, tellus pellentesque porttitor dignissim, sapien neque elementum risus, ac commodo erat massa nec lectus. Maecenas ut lobortis eros, gravida finibus neque. Nam ultrices leo nec efficitur accumsan. Mauris non venenatis nisl.', 'juris-page', '2019-04-25 08:49:48', '2019-04-25 08:49:48', 2),
(8, 'Php OOP ', 'Litas lapas piemÄ“rs', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis, nisi sit amet condimentum maximus, ex eros tristique lacus, quis efficitur lectus massa a elit. Nulla fringilla porta velit, nec gravida justo bibendum at. Vestibulum lorem quam, vehicula eu erat eget, placerat tincidunt purus. Sed laoreet erat sit amet massa faucibus maximus. Donec scelerisque suscipit felis, vel bibendum erat feugiat at. Donec tempus, diam at ornare condimentum, nibh erat euismod mauris, et elementum est mauris non eros. Proin dapibus laoreet mi, eget elementum eros.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam diam est, lacinia eu congue id, vestibulum id augue. Duis faucibus lobortis dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc at felis ut mauris imperdiet egestas sed sit amet tortor. Morbi et pellentesque augue, volutpat porttitor elit. Integer quis porttitor diam. Fusce facilisis lacus ante, ac sagittis odio vulputate vehicula. Pellentesque ultrices ex ut nibh fermentum consectetur. Mauris euismod pellentesque tellus nec ultrices. Donec tincidunt, tellus pellentesque porttitor dignissim, sapien neque elementum risus, ac commodo erat massa nec lectus. Maecenas ut lobortis eros, gravida finibus neque. Nam ultrices leo nec efficitur accumsan. Mauris non venenatis nisl.', 'lita-page', '2019-04-24 12:50:17', '2019-04-24 12:50:17', 0),
(9, '3D salas modelis', '3D salas modeÄ¼a izstrÄde unity', ' Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam diam est, lacinia eu congue id, vestibulum id augue. Duis faucibus lobortis dui. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc at felis ut mauris imperdiet egestas sed sit amet tortor. Morbi et pellentesque augue, volutpat porttitor elit. Integer quis porttitor diam. Fusce facilisis lacus ante, ac sagittis odio vulputate vehicula. Pellentesque ultrices ex ut nibh fermentum consectetur. Mauris euismod pellentesque tellus nec ultrices. Donec tincidunt, tellus pellentesque porttitor dignissim, sapien neque elementum risus, ac commodo erat massa nec lectus. Maecenas ut lobortis eros, gravida finibus neque. Nam ultrices leo nec efficitur accumsan. Mauris non venenatis nisl.\r\n\r\nProin non laoreet nulla. Nulla congue tincidunt congue. Maecenas cursus pellentesque lacus, quis facilisis ligula tempus id. Aliquam vitae dui dolor. Quisque congue eleifend fringilla. Ut euismod orci dignissim vulputate viverra. Curabitur porta tincidunt leo ut fermentum. Aliquam venenatis leo in velit maximus ullamcorper. Nullam vel tempor dui, consequat luctus lacus. Aliquam nulla orci, molestie ac ornare quis, malesuada a nisl. Mauris dapibus orci quis neque dapibus, id luctus justo tristique. Praesent mattis tempor mauris ac mollis.\r\n\r\nAliquam nisi felis, accumsan nec rhoncus at, congue et dolor. Cras at luctus mi. Proin quis pharetra mauris. In non fermentum tellus. Curabitur sagittis pretium facilisis. Nullam eu maximus quam. Nulla nec nulla in ligula dictum mattis mattis at enim. Cras consectetur sollicitudin facilisis. Donec viverra lacinia nibh sit amet finibus. Vivamus in leo volutpat, finibus sapien nec, rutrum lacus. Phasellus vel massa et enim pulvinar tincidunt at nec odio. Curabitur nulla purus, accumsan id erat at, dapibus porttitor orci. Phasellus ac porttitor orci. Nulla nec fringilla tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'modris-page', '2019-04-24 12:49:19', '2019-04-24 12:49:19', 0),
(10, 'tests23', 'tests23233333', ' aha44r', 'test-page', '2019-04-25 12:53:14', '2019-04-25 12:53:14', 20),
(11, '', '', ' ', '', '2019-04-25 08:56:11', NULL, 0),
(12, 'E-adreses ievieÅ¡ana', 'E-adreses', 'laoasf ', 'e-adrese', '2019-04-25 09:02:48', NULL, 0),
(13, 'andis', 'andis', 'andis ', 'andis', '2019-04-25 09:07:45', NULL, 0),
(14, 'Loremis', 'Loremis', ' Loremis', 'Loremis', '2019-04-25 09:52:45', NULL, 6),
(15, 'rotaljieta', 'rotaljieta', ' rotaljieta', 'rotaljieta', '2019-04-25 09:52:53', NULL, 6);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_latvian_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_latvian_ci NOT NULL,
  `salt` varchar(32) COLLATE utf8_latvian_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_latvian_ci NOT NULL,
  `joined` datetime NOT NULL,
  `usergroup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `name`, `joined`, `usergroup`) VALUES
(6, 'modris', '6044e547d48050769d39998167c4aeaad49250db9ff09e9ee697cd5be1129834', '¿vÉ‡ªŒ[¦Ö\\Ö+¼ã&a]-”oÞïDÐU', 'slieka', '2019-04-23 23:44:15', 3),
(19, 'guest', '061c33c7350dfd3e90ad6ed77dd9ad5eaf803a0fc0bc8aad4498c064489b32ef', '¨œÜ³0D`óþ¤ödq\'$/Õ¥Bê[Þ±/Fú8', 'guest', '2019-04-24 14:46:16', 1),
(20, 'lietotajs', 'a0bb6f338b730dda124cee7f40b6d07cac35ec39e6b7248a0e66055b5e57529b', 'dÕßž+q\"A^®ßŒd\0­wÂ’6{[½oÜôN§–ñ\'', 'lietotajs', '2019-04-25 12:03:38', 1),
(21, 'admin', '4e2f97c78404f5782d55adc68ef1be0d85479045dd3214ef4c8773e9df7c6476', '²`n…‡cÍ–ÿZnä\'ù4{¸³‡§]Î·ê¡ý+k', 'admin', '2019-04-25 12:09:06', 2),
(22, 'moderators', '221f9098fd8a7529c93374edd40a78b95b3c0f6ccfb3b699531cf7e85799a501', 'Ù,Âí/¤ Nlƒ5Ì\n4áö]œØdU\"óòøãoÍÌ&', 'moderators', '2019-04-25 12:44:31', 4);

-- --------------------------------------------------------

--
-- Table structure for table `users_session`
--

CREATE TABLE `users_session` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hash` varchar(64) COLLATE utf8_latvian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_latvian_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_session`
--
ALTER TABLE `users_session`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users_session`
--
ALTER TABLE `users_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

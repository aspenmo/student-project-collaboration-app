<?php
require_once 'core/init.php';
require 'app/start.php';
require VIEW_ROOT . '/templates/header.php';

if(Input::exists()) {
	if(Token::check(Input::get('token'))) {
		$validate = new Validate ();
		$validation = $validate->check($_POST, array(
			'username' => array(
				'required' => true,
				'min' => 2,
				'max' => 20,
				'unique' => 'users'
			),
			'password' => array(
				'required' => true,
				'min' => 6
			),
			'password_again' => array(
				'required' => true,
				'matches' => 'password'
			),
			'name' => array(
				'required' => true,
				'min' => 2,
				'max' => 50
			)
		));

		if($validation->passed()) {
			$user = new User();

			$salt = Hash::salt(32);
			
			try {
				//massivs no laukiem ko veelamies ievietot
				$user->create(array(
					'username' => Input::get('username'),
					'password' => Hash::make(Input::get('password'), $salt),
					'salt' => $salt,
					'name' => Input::get('name'),
					'joined' => date('Y-m-d H:i:s'),
					'usergroup' => 1

				));

				Session::flash('home', 'Tu esi reģistrējies. Tagad vari ielogoties.');
				header('Location: index.php');

			} catch(Exception $e) {
				die($e->getMessage());
			}
		} else {
			foreach($validation->errors() as $error) {
				echo $error, '<br>';
			}
		}
	}
}

?>

<form action="" method="post">
	<div class="form-group row">
		<label for="username" class="col-sm-2 col-form-label">Lietotājvārds</label>
		<div class="col-sm-10">
			<input class="form-control" type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off">
		</div>
	</div>

	<div class="form-group row">
		<label for="password" class="col-sm-2 col-form-label">Ievadi savu paroli</label>
		<div class="col-sm-10">
			<input class="form-control" type="password" name="password" id="password">
		</div>
	</div>

	<div class="form-group row">
		<label class="col-sm-2 col-form-label" for="password_again">Ievadi savu paroli atkārtoti</label>
		<div class="col-sm-10">
			<input class="form-control" type="password" name="password_again" id="password_again">
		</div>
	</div>

	<div class="form-group row">
		<label class="col-sm-2 col-form-label" for="name">Ievadi vārdu</label>
		<div class="col-sm-10">
			<input class="form-control" type="text" name="name" value="<?php echo escape(Input::get('name')); ?>" id="name"">
		</div>
	</div>

	<input type="hidden" name="token" value="<?php echo Token::generate();?>">
	<input class="btn btn-primary" type="submit" value="Reģistrēties">
</form>

<?php require VIEW_ROOT . '/templates/footer.php'; ?>
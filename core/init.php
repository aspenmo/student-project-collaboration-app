<?php
session_start();
$rootFolder = '/collaboration/';
//
$GLOBALS['config'] = array(
	'mysql' => array(
		'host' => '127.0.0.1',
		'username' => 'root',
		'password' => '',
		'db' => 'cms'

	),
	//cookie vardi un cookie expire, cik userus ilgi atceramies
	'remember' => array(
		'cookie_name' => 'hash',
		'cookie_expiry' => 604800
	),
	//session name un token izmantojamais
	'session' => array(
			'session_name' => 'user',
			'token_name' => 'token'
	)

);

//padodam funkciju, kas palaizaas katru reizi, kad klase ir accessed. Tad no argumentu saraksta sai funkcijai  panemam class name un tad varam require vinu. Panemam 10 koda rindas un saaiisinam taas uz dazaam





/*
spl_autoload_register(function($class){
	require_once 'classes/' . $class . '.php';
});

require_once 'functions/sanitize.php';
*/


spl_autoload_register(function($class){
	require_once __DIR__. '/../classes/' . $class . '.php';
});

require_once __DIR__. '/../functions/sanitize.php';












if(Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name'))) {
		$hash = Cookie::get(Config::get('remember/cookie_name'));
		$hashCheck = DB::getInstance()->get('users_session', array('hash', '=', $hash));

		if($hashCheck->count()) {
			$user = new User($hashCheck->first()->user_id);
			$user->login();
		}
	}
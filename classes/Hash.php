<?php
class Hash {
	//uztaisa hashu
	public static function make($string, $salt = '') {
		return hash('sha256', $string . $salt);
	}

	//uztaisa salt
	public static function salt($length) {
		return random_bytes($length);
		//return bin2hex(random_bytes($length));
		//return mcrypt_create_iv($length);
	}

	//uztaisa unique hash
	public static function unique() {
		return self::make(uniqid());
	}
}
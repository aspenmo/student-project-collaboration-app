<?php

require '../app/start.php';
require_once "../core/init.php";
$user = new User();
if(!$user->isLoggedIn()) {
	header("Location: http://".$_SERVER['HTTP_HOST'].$rootFolder.'login.php');
}

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
if(isset($_FILES['file'])) {
	$file = $_FILES['file'];


	$file_name = $file['name'];
	$file_tmp = $file['tmp_name'];
	$file_size = $file['size'];
	$file_error = $file['error'];

	$file_ext = explode('.', $file_name);
	$file_ext = strtolower(end($file_ext));
	$allowed = array('zip');

	if(in_array($file_ext, $allowed)) {
		if($file_error === 0) {
			if($file_size <= 20000000) {
				$file_name_new = uniqid('', true) . '.' . $file_ext;
				$file_destination = '../public/uploads/' . $file_name_new;

				if(move_uploaded_file($file_tmp, $file_destination)) {
					$file_destination;
				}
			}
		}

	}
}

if (!empty($_POST)) {
	$id = 		$_POST['id'];
	$user = new User();
	$user_id = 	escape($user->data()->id);
	$commit = 	!empty($_POST['commit_text']) ? $_POST['commit_text'] : 's';

	$updateAttachments = "INSERT INTO attachments (url, page_id, user_id,commit_text) VALUES (?,?,?,?)";
	$db->prepare($updateAttachments)->execute([$file_name_new, $id, $user_id, $commit]);

	header('Location: ' . BASE_URL . '/admin/upload.php');
}

if (!isset($_GET['id'])) {
	header('Location: ' . BASE_URL . '/admin/list.php');
	die();
}

$page = $db->prepare ("
	SELECT id, title, label, body, slug
	FROM pages 
	WHERE id = :id
");

$page->execute(['id' => $_GET['id']]);

$page = $page->fetch(PDO::FETCH_ASSOC);

require VIEW_ROOT . '/admin/upload.php';
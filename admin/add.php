<?php

require '../app/start.php';
require_once "../core/init.php";

if (!empty($_POST)) {
	$label = 	$_POST['label'];
	$title = 	$_POST['title'];
	$slug = 	$_POST['slug'];
	$body = 	$_POST['body'];
	$user = new User();
	$user_id = 	escape($user->data()->id);

	$insertPage = $db->prepare("
		INSERT INTO pages (label, title, slug, body, created, user_id) 
		VALUES (:label, :title, :slug, :body, NOW(), :user_id)
	");

	$insertPage->execute([
		'label' => $label,
		'title' => $title,
		'slug' => $slug,
		'body' => $body,
		'user_id' => $user_id
	]);

	header('Location: ' . BASE_URL . '/admin/list.php');
}

require VIEW_ROOT . '/admin/add.php';

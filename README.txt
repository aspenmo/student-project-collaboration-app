Uzstādīšana.

-----------1----------

Neaizmirstam ieslēgt apache un phpmyadmin servisus XAMPP'am vai releventai programmai!

-----------2----------

Izveidot savā phpmyadmin datubāzi ar nosaukumu "cms"

-----------3----------

Importēt pēdējo .sql cms tabulā. .sql Fails atrodams /sql mapē

-----------4----------

Samainīt pēc savas konfigurācijas datubāzes savienojumu datus: core/init.php

-----------5----------

Kā arī samainīt tos \app\start.php. Pievērst uzmanību arī start.php atrodamajam BASE_URL. Nomainīt korektu ceļu uz jūsu projekta failu. Piemēram, ja izmanto Windows xampp, tad htdocs būs mātes mape. Ja būsiet ielikuši uzreiz htdocs mapītē visu projektu, tad jāraksta http://localhost

Taču, ja esat iekš htdocs ielikuši projektu atsevišķā mapītē, piemēram, kā es. Tad būs http://localhost/collaboration

Kur "collaboration" ir mans lokālās mapes nosaukums.

-----------6----------

Piekļuves.

"Admin" pogai:
Lietotājvārds: admin
Parole: password

Parastajam login:
----moderators----
Lietotājvārds: moderators
Parole: password

----viesis jeb parasts lietotājs----
Lietotājvārds: lietotajs
Parole: password

----------------------------------------------------------------------------


Realizētā funkcionalitāte:
CMS
Realizēts atsevišķs admin panelis
Nodalīts viesu(publiskais) skats. Un reģistrētajiem lietotājiem savs papildinātais skats.
Dažādas lietotāju atļaujas. Ir vairākas lomas, admins, moderators un parastais lietotājs.
Projektu/dokumenta pievienošana un rediģēšana
Projektu izveides un atjauninājumu reģistrēšana
Iespēja definēt pēc savas izvēles URL slug (linku draudzīgo)
Iespēja adminam dzēst un rediģēt projektus, arī tos pievienot
Ielogošanās un reģistrācija
Sesija
Implementēta Cross-Site Request Forgery aizsardzība. Ģenerējam tokenus, kā arī viss tiek hashots un salted
Lietotāju profili
Projekts strukturizēts un pārvietošanās pa to realizēta lielākoties ar php skatiem
Izstrādāta sesijas cookie saglabāšana - lietotājs var izvēlēties sevi atcerēties un tam nebūs atkārtoti jāielogojās
Izmantota objektorientētā pieeja autorizācijas un reģistrācijas funkcionalitātei.


















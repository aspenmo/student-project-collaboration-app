<?php
require_once 'core/init.php';
require 'app/start.php';
require VIEW_ROOT . '/templates/header.php';

$user = new User();

if(!$user->isLoggedIn()) {
	Redirect::to('index.php');
}


if(Input::exists()) {
	if(Token::check(Input::get('token'))) {
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'name' => array (
				'required' => true,
				'min' => 2,
				'max' => 50
			)
		));

		if($validation->passed()) {
			try {
				$user->update(array(
					'name' => Input::get('name')
				));
				Session::flash('home', 'Tavi dati ir atjaunināti.');
				Redirect::to('index.php');
			} catch(Exception $e) {
				die($e->getMessage());
			}
		} else {
			foreach($validation->errors() as $error) {
				echo $error, '<br>';
			}
		}
	}
}
?>

<form action="" method="post">
	<div class="form-group row">
		<label class="col-sm-2 col-form-label" for="name">Pilnais vārds</label>
		<div class="col-sm-10">
			<input class="form-control" type="text" name="name" value="<?php echo escape($user->data()->name); ?>">
		</div>
	</div>
	<input class="btn btn-primary" type="submit" value="Atjaunināt">
	<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
</form>

<?php require VIEW_ROOT . '/templates/footer.php'; ?>
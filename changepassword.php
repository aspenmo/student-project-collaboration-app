<?php
require_once 'core/init.php';
require 'app/start.php';
require VIEW_ROOT . '/templates/header.php';
$user = new User();

if(!$user->isLoggedIn()) {
	Redirect::to('index.php');
}

if(Input::exists()) {
	//parbaudam tokenu, izvairamies no coross site forgery
	//taa, ko padod forma
	if(Token::check(Input::get('token'))) {
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'password_current' => array(
				'required' => true,
				'min' => 6
			),
			'password_new' => array(
				'required' => true,
				'min' => 6
			),
			'password_new_again' => array(
				'required' => true,
				'min' => 6,
				'matches' => 'password_new'
			)				
		));

		if($validation->passed()) {
			if(Hash::make(Input::get('password_current'), $user->data()->salt) !== $user->data()->password) {
					echo 'Tava pašreizējā parole ir nepareiza.';
			} else {
				$salt = Hash::salt(32);
				$user->update(array(
					'password' => Hash::make(Input::get('password_new'), $salt),
					'salt' => $salt

				));

				Session::flash('home', 'Tava parole ir nomainīta!');
				Redirect::to('index.php');
			}

		} else {
			foreach($validation->errors() as $error) {
				echo $error, '<br>';
			}
		}
	}
}
?>

<form action="" method="post">
	<div class="form-group row">
		<label class="col-sm-2 col-form-label" for="password_current">Pašreizējā parole</label>
		<div class="col-sm-10">
			<input class="form-control" type="password" name="password_current" id="password_current">
		</div>
	</div>

	<div class="form-group row">
		<label class="col-sm-2 col-form-label" for="password_new">Jauna parole</label>
		<div class="col-sm-10">
			<input class="form-control" type="password" name="password_new" id="password_new">
		</div>
	</div>

	<div class="form-group row">
		<label class="col-sm-2 col-form-label" for="password_new_again">Jaunā parole vēlreiz</label>
		<div class="col-sm-10">
			<input class="form-control" type="password" name="password_new_again" id="password_new_again">
		</div>
	</div>

	<input class="btn btn-primary" type="submit" value="Mainīt">
	<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
</form>

<?php require VIEW_ROOT . '/templates/footer.php'; ?>
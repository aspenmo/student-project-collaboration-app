<?php

require 'app/start.php';
require_once 'core/init.php';

//izvelkam visas lapas no datubazes tabulas
//izmantojam metodi fetchAll, lai dabuujam visus ierakstus, ko iedod querijs
$pages = $db->query("
	SELECT id, label, slug
	FROM pages
")->fetchAll(PDO::FETCH_ASSOC);

//querija izvads
//var_dump($pages);

require VIEW_ROOT . '/home.php';

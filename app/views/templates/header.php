<!doctype html>
<html lang="lv">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo BASE_URL; ?>/css/app.css">
    <title>Student Project Collaboration App</title>
    <link rel="icon" type="image/png" href="http://icons.iconarchive.com/icons/goescat/macaron/64/gimp-icon.png" />
  </head>
  <body>

  <div class="container">
	  <div class="row">
	    <div class="col-sm-12">	
    
    <header class="header">
  		<!-- Image and text -->
  		<nav class="navbar navbar-light" style="background-color:gainsboro;">
  		  <a class="navbar-brand" href="<?php echo BASE_URL; ?>">
  		    <img src="http://icons.iconarchive.com/icons/goescat/macaron/64/gimp-icon.png" width="30" height="30" class="d-inline-block align-top" alt="">
  		    Student Project Collaboration App
  		  </a>
        <a class="nav-link btn btn-secondary" href="<?php echo BASE_URL; ?>/admin/list.php">Admin panel</a>
  		</nav>
    </header>	


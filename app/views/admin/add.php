<?php require VIEW_ROOT . '/templates/header.php'; ?>
<?php require_once "../core/init.php"; ?>
	<h2>Add page</h2>

	<form action="<?php echo BASE_URL; ?>/admin/add.php" method="POST" autocomplete="off">
		<div class="form-group">
			<label for="title">Title</label>
			<input style="width:100%;" class="form-control" type="text" name="title" id="title">

			<label for="label">Label</label>
			<input style="width:100%;" class="form-control" type="text" name="label" id="label">

			<label for="slug">Slug</label>
			<input style="width:100%;" class="form-control" type="text" name="slug" id="slug">

			<label for="body">Body</label>
			<textarea class="form-control" name="body" id="body" cols="30" rows="3"> </textarea>
			<hr>
			<input class="btn btn-primary" type="submit" value="Add">
		</div>
	</form>
<?php require VIEW_ROOT . '/templates/footer.php'; ?>
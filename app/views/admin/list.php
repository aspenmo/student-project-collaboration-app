<?php 
require VIEW_ROOT . '/templates/header.php'; 
require_once "../core/init.php";
$user = new User();
//if ($user->hasPermission('moderator') || $user->hasPermission('admin')) {
	//echo 'modins';
//} else {
	//header("Location: http://".$_SERVER['HTTP_HOST'].$rootFolder.'login.php');
//}
if(!$user->isLoggedIn()) {
	header("Location: http://".$_SERVER['HTTP_HOST'].$rootFolder.'login.php');
}



/*
if($user->isLoggedIn()) {
	echo 'logged in';
} else {
	echo 'logged out';
}
*/




if ($user->hasPermission('moderator') || $user->hasPermission('admin')) {
	$user_id = escape($user->data()->id);
	$pagesAdminList = $db->query("
		SELECT id, label, title, slug
		FROM pages
	")->fetchAll(PDO::FETCH_ASSOC);
} else if ($user->isLoggedIn() && !$user->hasPermission('moderator') && !$user->hasPermission('admin')) {
		$user_id = escape($user->data()->id);
		$pagesAdminList = $db->query("
			SELECT id, label, title, slug
			FROM pages
			WHERE user_id = $user_id
		")->fetchAll(PDO::FETCH_ASSOC);
}













?>










	<?php if (empty($pagesAdminList)): ?>
	<div class="alert alert-warning" role="alert">
	  No pages at the moment.
	</div>
	<?php else: ?>
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Projekta nosaukums</th>
						<th>Projekta virsraksts</th>
						<th>Saite</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($pagesAdminList as $page): ?>
						<tr>
							<td><?php echo e($page['label']); ?></td>
							<td><?php echo e($page['title']); ?></td>
							<td><a href="<?php echo BASE_URL; ?>/page.php?page=<?php echo e($page['slug'])?>"><?php echo e($page['slug']); ?></a></td>
							<td><a href="<?php echo BASE_URL; ?>/admin/edit.php?id=<?php echo e($page['id']) ?>">Rediģēt</a></td>
							<td>
							<?php
							if($user->hasPermission('admin')){
								echo "<a href=". BASE_URL . "/admin/delete.php?id=". e($page['id']).">Dzēst</a>";
							}
							?>
							</td>


						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	<?php endif; ?>

	<a class="btn btn-primary" href="<?php echo BASE_URL; ?>/admin/add.php">Izveidot jaunu projektu</a>









<?php require VIEW_ROOT . '/templates/footer.php'; ?>
<?php require VIEW_ROOT . '/templates/header.php'; ?>
<?php require_once "../core/init.php"; ?>
	<h2>Rediģēšana</h2>

	<form enctype="multipart/form-data" action="<?php echo BASE_URL; ?>/admin/edit.php" method="POST" autocomplete="off">
		<div class="form-group">
			<label for="title">Projekta virsraksts</label>
			<input style="width:100%;" class="form-control" type="text" name="title" id="title" value="<?php echo e($page['title']);?>">

			<label for="label">Projekta nosaukums</label>
			<input style="width:100%;" class="form-control" type="text" name="label" id="label" value="<?php echo e($page['label']);?>">

			<label for="slug">Pielāgotā saite</label>
			<input style="width:100%;" class="form-control" type="text" name="slug" id="slug" value="<?php echo e($page['slug']);?>">

			<label for="body">Apraksts</label>
			<textarea class="form-control" name="body" id="body" cols="30" rows="3"><?php echo e($page['body']); ?></textarea>
			<hr>
			<input type="hidden" name="id" value="<?php echo e($page['id']); ?>">

			<input class="btn btn-primary" type="submit" value="Rediģēt">
		</div>
	</form>

	
<?php require VIEW_ROOT . '/templates/footer.php'; ?>
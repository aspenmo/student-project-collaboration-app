<?php require VIEW_ROOT . '/templates/header.php'; ?>
<?php require_once "../core/init.php"; 


?>
	<h2>Versiju augšupielāde</h2>

	<form enctype="multipart/form-data" action="<?php echo BASE_URL; ?>/admin/upload.php" method="POST" autocomplete="off">
		<div class="form-group">
			<h4><?php echo e($page['title']);?></h4><br>
			<label for="commit">Versijas komentārs</label>
			<textarea class="form-control" name="commit_text" id="commit" cols="30" rows="3"></textarea>
			<hr>
			<input type="hidden" name="id" value="<?php echo e($page['id']); ?>">

			<input type="file" name="file">
			<hr>

			<input class="btn btn-primary" type="submit" value="Augšupielādēt">
		</div>
	</form>

	
<?php require VIEW_ROOT . '/templates/footer.php'; ?>
<?php require VIEW_ROOT . '/templates/header.php'; ?>
<div class="row">
    <div class="col-sm-12 col-md-9">
		<?php //vai ir kaadas lapas ?>
		<?php if (empty($pages)): ?>
			<div class="alert alert-warning" role="alert">Sorry, no pages at the moment.</div>
		<?php else: ?>
			<div class="list-group">
				<?php foreach ($pages as $page): ?>
					<a class="list-group-item list-group-item-action" href="<?php echo BASE_URL; ?>/page.php?page=<?php echo $page['slug']; ?>"><?php echo $page['label']; ?></a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="col-sm-12 col-md-3" >
		<?php
		//$user = DB::getInstance()->update('users', 3,array(
		//	'password' => 'newpassword',
		//	'name' => 'Modris LApsA'
		//));

		//if(Session::exists('success')) {
			//echo Session::flash('success');
		//}

		if(Session::exists('home')) {
			echo '<p>' . Session::flash('home') . '</p>';
		}

		$user = new User();
		if($user->isLoggedIn()) {
			
		?>
		
		<div class="list-group">
			<li class="text-center list-group-item list-group-item-dark" aria-disabled="true">
				<p class="greeting">Sveiks <a href="profile.php?user=<?php echo escape($user->data()->username); ?>"><?php echo escape($user->data()->username);?></a>!<br></p>
			</li>
			<a href="index.php" class="list-group-item list-group-item-action">Sākums</a>
			<a href="profile.php?user=<?php echo escape($user->data()->username); ?>" class="list-group-item list-group-item-action">Profils</a>
			<a href="update.php" class="list-group-item list-group-item-action">Atjaunot datus</a>
			<a href="changepassword.php" class="list-group-item list-group-item-action">Mainīt paroli</a>
			<a href="logout.php" class="list-group-item list-group-item-action">Iziet</a>
		</div>
		<?php
		if($user->hasPermission('moderator')) {
			echo '<div class="text-center"><span class="text-center badge badge-pill badge-primary">Moderators<span class="badge badge-primary"></div>';
		}

		} else {
			echo '<p>Tev vajag <a href="login.php">ielogoties</a> vai <a href="register.php">reģistrēties</a></p>';
		}

		?>
	</div>
</div>



<?php require VIEW_ROOT . '/templates/footer.php'; ?>
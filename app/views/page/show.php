<?php require VIEW_ROOT . '/templates/header.php'; ?>

	<?php if (!$page): ?>
			<div class="alert alert-warning" role="alert">No page found, sorry.</div>
	<?php else: ?>

		<h2><?php echo e($page['title']); ?></h2>

		<?php echo e($page['body']); ?>
		<hr>
		<p class="text-muted">
			Created on <?php echo $page['created']->format('jS M Y h:i a'); ?>
			<?php if ($page['updated']): ?>
				<br>
				Last updated <?php echo $page['updated']->format('jS M Y h:i a'); ?>
			<?php endif; ?>
		</p>
		<hr>
<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Projekta versiju kontrole</h1>
    <p class="lead">Pievieno šim projektam savu versiju un apskati pārējās</p>
    <?php echo '
	 <a class="btn btn-primary" href="'. $rootFolder . 'admin/upload.php?id=' . $page['id'] . '" role="button">Pievienot versiju (.zip)</a>
 '; ?>
  </div>
</div>
		
		<div class="row">

			<?php if ($pageCommits) { ?>
				<?php $i=1; foreach($pageCommits as $ticket) { ?>
					<div class="col-sm-4">
						<div class="card bg-light mb-3" style="max-width: 18rem;">
							<div class="card-header">
								 Lietotājs Nr: <?php echo $ticket['user_id']; ?>	
							</div>
							<div class="card-body">
						        <h5 class="card-title"></h5>
						        <p class="card-text"><?php echo $ticket['commit_text']; ?></p>
						        <p><?php echo $ticket['created'];?></p>
								<?php echo '
								<form method="get" action="'. $rootFolder . 'public/uploads/' . $ticket['url'] . '">
								   <button class="btn btn-outline-secondary" type="submit">Lejupielādēt</button>
								</form>
								'
								?>
					      	</div>
				      	</div>
					</div>
				<?php }
			} else { ?>
				

			<?php } ?>
		</div>

	<?php endif; ?>

<?php require VIEW_ROOT . '/templates/footer.php'; ?>